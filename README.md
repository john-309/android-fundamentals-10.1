# Android Fundamentals 10.1

Android Fundamentals 10.1 is a tutorial that teaches app developers 
the basics of Room, LiveData, and the ViewModel. Part A of the tutorial focuses 
on implementing these resources by creating an app, while part B explains how to 
delete data from a Room database. This project was built using Android Studio and made possible by following the 
online tutorials at [Google Codelabs](https://codelabs.developers.google.com/)  

Here is the link for Part A:  
https://codelabs.developers.google.com/codelabs/android-training-livedata-viewmodel/index.html?index=..%2F..android-training#2

Here is the link for Part B:  
https://codelabs.developers.google.com/codelabs/android-training-room-delete-data/index.html?index=..%2F..android-training#0  
